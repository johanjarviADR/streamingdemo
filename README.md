# Streaming Demo App

## How to setup Gstreamer Binaries

In order to install the Gstreamer binaries for Android [this link](https://gstreamer.freedesktop.org/documentation/installing/for-android-development.html?gi-language=c) can be very useful.

The place to download the binaries [can be found here](https://gstreamer.freedesktop.org/data/pkg/android/). We are using version `1.18.3` currently.

What you need to do is download the binaries and unzip them to a directory on your machine. You can then open the Android project and inside `gradle.properties` in the root directory there is a line at the bottom that reads:
`gstAndroidRoot=/home/johan/Android/gstreamer`

Obviously this path would be different if not using a Linux system.

## Things to note

Currently we use Gstreamer and it is a particular way that things are set up. You can read about all the various pipeline settings online but ultimately in `line 135` of `video.c` you can see the current pipeline that is being used in order to handle our video feed.

A testing prototype board will be provided that has a running GoPro, a raspberry pi, a wifi access point and a running Gstreamer pipeline on that network.

This will be setup to run through a wireless local network at all times and with it's current settings on the Raspberry Pi and the code in its state we are several seconds worth of latency on the App and we need it to be down to the milliseconds, as close to real-time as humanly possible.
