package au.com.australiandroid.streaming_demo;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import org.freedesktop.gstreamer.GStreamer;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    // Native setup
    static {
        System.loadLibrary("gstreamer_android");
        System.loadLibrary("video");
        nativeClassInit();
    }

    private native void nativeInit(String pipelineString);     // Initialize native code, build pipeline, etc
    private native void nativePlay();     // Set pipeline to PLAYING

    @SuppressWarnings("unused") // There if we need it and exists in C code.
    private native void nativePause();    // Set pipeline to PAUSED

    private static native boolean nativeClassInit(); // Initialize native class: cache Method IDs for callbacks
    private native void nativeSurfaceInit(Object surface);
    private native void nativeSurfaceFinalize();
    private native void nativeFinalize(); // Destroy pipeline and shutdown native code

    @SuppressWarnings("unused") // we will likely use this soon when we add custom pipeline settings
    private long native_custom_data;      // Native code will use this to keep private data

    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGStreamer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        nativeFinalize();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        // TODO: Give this a real pipeline string input instead of having it  hardcoded in Video.c
        nativeInit("");
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    /**
     * Initialises everything view related to G Streamer.
     */
    private void initGStreamer() {
        try {
            GStreamer.init(this);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        SurfaceView sv = this.findViewById(R.id.surface_video);
        SurfaceHolder sh = sv.getHolder();
        sh.addCallback(this);
        nativePlay();
    }

    // Called from native code. Native code calls this once it has created its pipeline and
    // the main loop is running, so it is ready to play.
    @SuppressWarnings("unused")
    private void onGStreamerInitialized () {
        nativePlay();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d("GStreamer",
                "Surface changed to format " + format + " width " + width + " height " + height);
        nativeSurfaceInit (holder.getSurface());
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.d("GStreamer", "Surface created: " + holder.getSurface());
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("GStreamer", "Surface destroyed");
        nativeSurfaceFinalize ();
    }

    /**
     * Parameters to set to hide the system UI.
     */
    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}